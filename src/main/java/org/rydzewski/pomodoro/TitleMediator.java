/**
 * 
 */
package org.rydzewski.pomodoro;

/**
 * @author Mikolaj Rydzewski
 *
 */
public interface TitleMediator {
	
	final static String TITLE = "Pomodoro";

	void setTitle(String title);
	
	void activate();
	
}
