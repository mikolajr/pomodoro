package org.rydzewski.pomodoro;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class ImagePane extends JPanel {

	private static final long serialVersionUID = -5455832037103425866L;
	
	private BufferedImage image;
	
	public ImagePane(String imageName) {
		super();
		InputStream is = null;
		try {
			is = getClass().getClassLoader().getResourceAsStream(imageName);
			this.image = ImageIO.read(is);
			setPreferredSize(new Dimension(this.image.getWidth(), this.image.getHeight()));
		} catch (IOException e) {
		}
		finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
				}
			}
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		int x = getWidth()/2 - image.getWidth()/2;
		int y = getHeight()/2 - image.getHeight()/2;
		g.drawImage(image, x, y, null);
	}

}
