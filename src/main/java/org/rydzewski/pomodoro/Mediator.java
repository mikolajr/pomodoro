/**
 * 
 */
package org.rydzewski.pomodoro;

/**
 * @author Mikolaj Rydzewski
 *
 */
public interface Mediator {

	void update(int value, int max);
	
	void alarm();
}
