package org.rydzewski.pomodoro;
import javax.swing.JApplet;


public class PomodoroApplet extends JApplet implements TitleMediator {

	private static final long serialVersionUID = 8215795562192452146L;
	
	private PomodoroPane pomodoroPane;

	@Override
	public void init() {
		super.init();
		pomodoroPane = new PomodoroPane(this);
		getContentPane().add(pomodoroPane);
		resize(400, 400);
	}

	@Override
	public void setTitle(String title) {
	}

	@Override
	public void destroy() {
		pomodoroPane.shutdown();
		super.destroy();
	}

	@Override
	public void activate() {
	}

	
}
