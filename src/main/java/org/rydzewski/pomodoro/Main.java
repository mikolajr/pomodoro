/**
 * 
 */
package org.rydzewski.pomodoro;

import java.awt.HeadlessException;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 * @author Mikolaj Rydzewski
 * 
 */
public class Main extends JFrame implements TitleMediator {

	private static final long serialVersionUID = -7299389083996554861L;

	private PomodoroPane pomodoroPane;

	public Main() throws HeadlessException {
		super(TITLE);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		pomodoroPane = new PomodoroPane(this);
		add(pomodoroPane);
		pack();

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}

	@Override
	public void dispose() {
		pomodoroPane.shutdown();
		super.dispose();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new Main().setVisible(true);
			}
		});
	}

	@Override
	public void activate() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				setExtendedState(JFrame.NORMAL);
				toFront();
				repaint();
			}
		});
	}
}
