/**
 * 
 */
package org.rydzewski.pomodoro;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author Mikolaj Rydzewski
 * 
 */
public class PomodoroService {

	private final static int POMODORO_LEN = 25 * 60;
	private final static int SHORT_BREAK_LEN = 5 * 60;
	private final static int LONG_BREAK_LEN = 10 * 60;

	private final Mediator mediator;

	private int max;
	private int value;
	private ExecutorService executor;
	private Future<?> pomodoroTask;

	public PomodoroService(Mediator mediator) {
		this.mediator = mediator;
		executor = Executors.newSingleThreadExecutor();
	}

	public int pomodoro() {
		stop();
		start(0, POMODORO_LEN);
		return max;
	}

	public int longBreak() {
		stop();
		start(0, LONG_BREAK_LEN);
		return max;
	}

	public int shortBreak() {
		stop();
		start(0, SHORT_BREAK_LEN);
		return max;
	}

	private synchronized void start(final int value, final int max) {
		pomodoroTask = executor.submit(new Runnable() {
			@Override
			public void run() {
				int v = value;
				int m = max;
				PomodoroService.this.value = v;
				PomodoroService.this.max = m;
				
				mediator.update(v, m);

				while (v < m) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						return;
					}
					if (Thread.interrupted()) {
						return;
					}

					v++;
					PomodoroService.this.value = v;
					mediator.update(v, m);
				}
				
				mediator.alarm();
			}
		});
	}

	public void start() {
		start(value, max);
	}

	public synchronized void stop() {
		if (pomodoroTask != null) {
			pomodoroTask.cancel(true);
			pomodoroTask = null;
		}
	}

	public void reset() {
		value = 0;
		mediator.update(value, max);
	}

	public int getMax() {
		return max;
	}

	public void shutdown() {
		stop();
		executor.shutdown();
		executor.shutdownNow();
	}

}
