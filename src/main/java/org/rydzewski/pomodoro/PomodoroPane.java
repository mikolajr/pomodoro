/**
 * 
 */
package org.rydzewski.pomodoro;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

/**
 * @author Mikolaj Rydzewski
 * 
 */
public class PomodoroPane extends JPanel implements Mediator {

	private static final long serialVersionUID = -4297921400457275539L;

	private static final int ALARM_REPEAT_COUNT = 2;
	private static final String PHONE_WAV = "phone.wav";
	private JProgressBar bar;
	private PomodoroService pomodoroService;
	private ClipWrapper clip;
	private final TitleMediator titleMediator;

	public PomodoroPane(TitleMediator titleMediator) {
		this.titleMediator = titleMediator;
		pomodoroService = new PomodoroService(this);

		ImagePane imagePane = new ImagePane("pomodoro.png");
		imagePane.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				synchronized (PomodoroPane.this) {
					if (clip != null) {
						clip.cancel();
					}
				}
			}
		});
		setLayout(new BorderLayout());
		add(imagePane, BorderLayout.CENTER);
		add(createControlPane(), BorderLayout.NORTH);
		bar.setPreferredSize(new Dimension(300, 80));
	}

	private Component createControlPane() {
		// top panel with buttons
		JPanel buttons = new JPanel();

		JButton button = new JButton("Pomodoro");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				bar.setMaximum(pomodoroService.pomodoro());
			}
		});
		buttons.add(button);

		button = new JButton("Short Break");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				bar.setMaximum(pomodoroService.shortBreak());
			}
		});
		buttons.add(button);

		button = new JButton("Long Break");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				bar.setMaximum(pomodoroService.longBreak());
			}
		});
		buttons.add(button);

		// middle panel with progress bar
		JPanel progress = new JPanel();
		bar = new JProgressBar(SwingConstants.HORIZONTAL, 0, pomodoroService
				.getMax());
		bar.setString("");
		bar.setStringPainted(true);
		bar.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 60));
		progress.add(bar);

		// lower panel with controls
		JPanel controls = new JPanel();

		button = new JButton("Start");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				pomodoroService.start();
			}
		});
		controls.add(button);

		button = new JButton("Stop");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				pomodoroService.stop();
			}
		});
		controls.add(button);

		button = new JButton("Reset");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				pomodoroService.reset();
			}
		});
		controls.add(button);

		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.add(buttons, BorderLayout.NORTH);
		panel.add(progress, BorderLayout.CENTER);
		panel.add(controls, BorderLayout.SOUTH);

		return panel;
	}

	@Override
	public void update(final int value, final int max) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				bar.setMaximum(max);
				bar.setValue(value);

				int remaining = max - value;
				String time = String.format("%d:%02d", remaining / 60,
						remaining % 60);
				titleMediator.setTitle(time + " - " + TitleMediator.TITLE);
				bar.setString(time);
			}
		});
	}

	public void shutdown() {
		pomodoroService.shutdown();
	}

	@Override
	public synchronized void alarm() {
		try {
			clip = new ClipWrapper(PHONE_WAV);
			clip.play(ALARM_REPEAT_COUNT);
			titleMediator.activate();
		} catch (Exception ignored) {
		}
	}

}
