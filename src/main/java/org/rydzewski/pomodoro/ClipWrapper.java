/**
 * 
 */
package org.rydzewski.pomodoro;

import java.io.IOException;
import java.net.URL;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * @author Mikolaj Rydzewski
 * 
 */
public class ClipWrapper {

	private Clip clip;
	private Thread reaper;

	public ClipWrapper(String name) throws LineUnavailableException,
			UnsupportedAudioFileException, IOException {
		URL url = getClass().getClassLoader().getResource(name);
		AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(url);
		clip = AudioSystem.getClip();
		clip.open(audioInputStream);
	}
	
	public synchronized void play(final int repeatCount) {
		clip.start();
		clip.loop(repeatCount);
		reaper = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(repeatCount * clip.getMicrosecondLength() / 1000);
				} catch (InterruptedException e) {
				}
				clip.stop();
				clip.close();
				clip = null;
				reaper = null;
			}});
		reaper.setDaemon(true);
		reaper.start();
	}
	
	public synchronized  void cancel() {
		if (reaper != null) {
			reaper.interrupt();
		}
	}

	@Override
	protected void finalize() throws Throwable {
		if (clip != null) {
			clip.stop();
			clip.close();
		}
		super.finalize();
	}
	
}
